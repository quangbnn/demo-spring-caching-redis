package vn.tcx.demo.springcachingredis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.tcx.demo.springcachingredis.domain.NhanVien;

@Repository
public interface NhanVienRepository extends JpaRepository<NhanVien, Long>{

}
