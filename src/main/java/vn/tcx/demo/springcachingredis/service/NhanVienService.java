package vn.tcx.demo.springcachingredis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import vn.tcx.demo.springcachingredis.domain.NhanVien;
import vn.tcx.demo.springcachingredis.repository.NhanVienRepository;

@Service
public class NhanVienService {

    @Autowired
    private NhanVienRepository nhanVienRepository;

    @Cacheable(value = "nhanVienCache", key = "#id")
    public NhanVien findNhanVienById(Long id) {
        System.out.println("get Nhan Vien By Id");

        return nhanVienRepository.findById(id).get();
    }

    @Cacheable(value = "allNhanVienCache", unless = "#result.size() == 0")
    public List<NhanVien> findAllNhanVien() {
        System.out.println("find All Nhan Vien");

        return nhanVienRepository.findAll();
    }

    @Caching(put = { @CachePut(value = "nhanVienCache", key = "#nhanVien.id") }, 
            evict = { @CacheEvict(value = "allNhanVienCache", allEntries = true) })
    public NhanVien createNhanVien(NhanVien nhanVien) {
        System.out.println("create Nhan Vien");

        return nhanVienRepository.save(nhanVien);
    }

    @Caching(put = { @CachePut(value = "nhanVienCache", key = "#nhanVien.id") }, 
            evict = { @CacheEvict(value = "allNhanVienCache", allEntries = true) })
    public NhanVien updateNhanVien(NhanVien nhanVien) {
        System.out.println("update Nhan Vien");

        return nhanVienRepository.save(nhanVien);
    }

    @Caching(evict = { @CacheEvict(value = "nhanVienCache", key = "#nhanVien.id"),
            @CacheEvict(value = "allNhanVienCache", allEntries = true) })
    public void deleteNhanVien(Long id) {
        System.out.println("delete Nhan Vien");

        nhanVienRepository.deleteById(id);
    }

    @Caching(evict = { @CacheEvict(value = "nhanVienCache"),
            @CacheEvict(value = "allNhanVienCache") })
    public void clearCache() {
        System.out.println("Deleted cache");
    }
}
