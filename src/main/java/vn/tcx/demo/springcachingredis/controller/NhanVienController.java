package vn.tcx.demo.springcachingredis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import vn.tcx.demo.springcachingredis.domain.NhanVien;
import vn.tcx.demo.springcachingredis.service.NhanVienService;

@RestController
@RequestMapping("/api")
public class NhanVienController {

    @Autowired
    private NhanVienService nhanVienService;

    @GetMapping(value = "/nhan-vien/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void findNhanVienById(@PathVariable(value = "id") Long id) {
        nhanVienService.findNhanVienById(id);
    }

    @GetMapping(value = "/nhan-vien")
    @ResponseStatus(HttpStatus.OK)
    public List<NhanVien> findAllNhanVien() {
        return nhanVienService.findAllNhanVien();
    }

    @PostMapping(value = "/nhan-vien")
    @ResponseStatus(HttpStatus.CREATED)
    public void createNhanVien(@RequestBody NhanVien nhanVien) {
        nhanVienService.createNhanVien(nhanVien);
    }

    @PutMapping(value = "/nhan-vien/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateNhanVien(@PathVariable(value = "id") Long id,
            @RequestBody NhanVien nhanVien) {
        nhanVienService.updateNhanVien(nhanVien);
    }

    @DeleteMapping(value = "/nhan-vien/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteNhanVien(@PathVariable(value = "id") Long id) {
        nhanVienService.deleteNhanVien(id);
    }
    
    @DeleteMapping(value = "/nhan-vien")
    @ResponseStatus(HttpStatus.OK)
    public void clearCache() {
        nhanVienService.clearCache();
    }
}
