package vn.tcx.demo.springcachingredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringCachingRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringCachingRedisApplication.class, args);
	}

}
