package vn.tcx.demo.springcachingredis.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@Configuration
@EnableCaching
@PropertySource("classpath:application.properties")
public class RedisConfig {

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {
        return new JedisConnectionFactory();

    }

    @Bean
    public RedisCacheManager cacheManager() {
        RedisCacheManager redisCacheManager = RedisCacheManager.create(redisConnectionFactory());

        redisCacheManager.setTransactionAware(true);

        return redisCacheManager;
    }

//    @Bean
//    public RedisTemplate<String, NhanVien> redisTemplate() {
//        RedisTemplate<String, NhanVien> redisTemplate = new RedisTemplate<String, NhanVien>();
//
//        redisTemplate.setConnectionFactory(redisConnectionFactory());
//
//        return redisTemplate;
//    }

}
